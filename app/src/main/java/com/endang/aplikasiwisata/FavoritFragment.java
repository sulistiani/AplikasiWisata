package com.endang.aplikasiwisata;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritFragment extends Fragment {

    RecyclerView recyclerView;
    DatabaseHelper database;
    ArrayList<WisataModel> listWisataFavorite;
    public FavoritFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Wisata Favorite");
        return inflater.inflate(R.layout.fragment_favorit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);

        database = new DatabaseHelper(getActivity());

        listWisataFavorite = new ArrayList<>();
        listWisataFavorite = database.getDataFavorite();

        WisataAdapter adapter = new WisataAdapter(getActivity(),listWisataFavorite);
        recyclerView.setAdapter(adapter);


    }


}