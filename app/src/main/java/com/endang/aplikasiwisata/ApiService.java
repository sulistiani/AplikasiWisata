package com.endang.aplikasiwisata;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("read_wisata.php")
    Call<ListWisataModel> ambilDataWisata();
}