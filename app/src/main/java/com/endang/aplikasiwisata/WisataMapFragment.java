package com.endang.aplikasiwisata;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * A simple {@link Fragment} subclass.
 */
public class WisataMapFragment extends Fragment implements OnMapReadyCallback {


    public WisataMapFragment() {
        // Required empty public constructor
    }

    private GoogleMap mMap;
    private SupportMapFragment mapFrag;
    private static final String TAG = "WisataMapFragment";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Peta Wisata");
        View view = inflater.inflate(R.layout.fragment_wisata_map, container, false);

        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapViewWisata);
        mapFrag.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng semarang = new LatLng(-6.966667, 110.416664);
        mMap.addMarker(new MarkerOptions().position(semarang).title("Marker in Semarang"));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(semarang,10));

    }


}
